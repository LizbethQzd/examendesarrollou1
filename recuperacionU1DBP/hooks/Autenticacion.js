import { enviar } from "./Conexion";
import { save, saveToken } from "./SessionUtils";

export async function inicio_sesion(data) {
    const sesion = await enviar('examen.php', data)
    console.log(sesion.info);
    if (sesion && sesion.code == 200 && sesion.info.code) {
        saveToken(sesion.info.code);
        save('id', sesion.info.external);
        save('user', sesion.info.dni);
    }
    return sesion;
}