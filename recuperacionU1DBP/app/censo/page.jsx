'use client';
import React, { useState, useEffect } from 'react';
import { getToken } from '@/hooks/SessionUtilsClient';
import { ObtenerD } from '@/hooks/Conexion';
import Link from 'next/link';

export default function Page() {

    const [censos, setCensos] = useState([]);


    useEffect(() => {
        const registros = async () => {
            try {
                const token = getToken();
                const usuarios = extUsuario();
                if (!usuarios) {
                    return;
                }
                const url = `examen.php/?resource=census_children_login&external=${usuarios}`;
                const resp = await ObtenerD(url, token);
                const resultado = resp.info;
                // console.log(resultado);
                setCensos(resultado);
            } catch (error) {
                console.error('Error:', error);
            }
        };

        registros();
    }, []);



    /* const AccionesMaterias = () => {
         <div style={{ display: 'flex', gap: '10px' }}>
                                         <Link href="/censo/editar/[external]" as={`censo/editar/${dato.extrenal_censo}`} className="btn btn-outline-succes btn-rounded" aria-label="Editar Censo">
                                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                                                 <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                                 <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                             </svg>
                                             <span>EDITAR</span>
                                         </Link>
                                     </div>
     };*/



    function extUsuario() {
        const external = sessionStorage.getItem('id');
        return external;
    }

    return (
        <div className="row">
            <figure className="text-center">
                <h1>LISTA DE NIÑOS SENSADOS</h1>
            </figure>
            <div className="container-fluid">
                <div className="col-4">
                    <Link href="/censo/registrar" className="btn btn-success">REGISTRAR</Link>
                </div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Nro</th>
                            <th>NOMBRES</th>
                            <th>EDAD</th>
                            <th>TALLA</th>
                            <th>PESO</th>
                            <th>REPRESENTANTE</th>
                            <th>ACTIVIDADES</th>
                            <th>ESCUELA</th>
                            <th>CURSO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        {censos.map((dato, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{dato.nombres}</td>
                                <td>{dato.edad}</td>
                                <td>{dato.talla}</td>
                                <td>{dato.peso}</td>
                                <td>{dato.representante}</td>
                                <td>{dato.actividades}</td>
                                <td>{dato.escuela}</td>
                                <td>{dato.curso}</td>
                                <td>
                                <div style={{ display: 'flex', gap: '10px' }}>
                                         <Link href="/censo/editar/[external]" as={`censo/editar/${dato.extrenal_censo}`} className="btn btn-outline-succes btn-rounded" aria-label="Editar Censo">
                                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                                                 <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                                 <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                             </svg>
                                             <span>EDITAR</span>
                                         </Link>
                                     </div>


                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

            </div>
        </div>
    );
};