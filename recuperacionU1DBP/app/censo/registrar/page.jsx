'use client';
import React, { useState, useEffect } from 'react';
import { obtener, enviar, ObtenerD } from '@/hooks/Conexion';
import mensajes from '@/componentes/Mensajes';
import { getToken } from '@/hooks/SessionUtilsClient';
import { useRouter } from 'next/navigation';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";


export default function Page() {

    //VALIDACIONES DE LOS DATOS
    const validationSchema = Yup.object().shape({
        weight: Yup.string().required('Ingrese el peso'),
        height: Yup.string().required('Ingrese la altura'),
        representative: Yup.string().required('Ingrese el resentante'),
        activities: Yup.string().required('Ingrese la actividad'),

    });
    const [cursos, setCursos] = useState([]);
    const [ninos, setNinos] = useState([]);
    const [escuelas, setEscuelas] = useState([]);


    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, setValue, handleSubmit, formState } = useForm(formOptions);
    const router = useRouter();
    const { errors } = formState;

    useEffect(() => {
        const obtenerNino = async () => {
            try {
                const token = getToken();
                const response = await ObtenerD('examen.php/?resource=unregistered_children', token);
                const rpt = response.info;
                setNinos(rpt)
                console.log(ninos.info);
            } catch (error) {
                console.error('Error:', error);
            }
        };

        obtenerNino();
    }, []);

    function xternalUsuario() {
        const external = sessionStorage.getItem('id');
        return external;
    }

    useEffect(() => {
        const obtenerCurso = async () => {
            try {
                const token = getToken();
                const response = await obtener('examen.php/?resource=course', token);
                const rpt = response.info;
                setCursos(rpt)
            } catch (error) {
                console.error('Error:', error);
            }
        };

        obtenerCurso();
    }, []);

    useEffect(() => {
        const obtenerEscuela = async () => {
            try {
                const token = getToken();
                const response = await ObtenerD('examen.php/?resource=school', token);
                const rpt = response.info;
                setEscuelas(rpt)
            } catch (error) {
                console.error('Error:', error);
            }
        };

        obtenerEscuela();
    }, []);
    const borderStyles = {
        border: '1px solid #ced4da',
        borderRadius: '5px',
        padding: '8px',
    };
    const onSubmit = (data) => {
        const externalUsuario = xternalUsuario();

        if (!externalUsuario) {
            return;
        }

        const Data = {
            "resource": "saveCensus",
            "weight": parseFloat(data.weight),
            "height": parseFloat(data.height),
            "representative": data.representative,
            "activities": data.activities,
            "external_child": data.external_child,
            "external_school": data.external_school,
            "external_course": data.external_course,
            "external_session": externalUsuario
        };

        const token = getToken();

        enviar('examen.php', Data, token).then((info) => {
            if (info === '') {
                mensajes("Error en inicio de sesion", info.msg, "error");
            } else {
                console.log(info);
                mensajes("Exelentee!", "SE HA REGISTRADO CON ÉXITO");
                router.push('/censo');
            }
        });
    };



    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content" >
                    <h1>Registrar Censos</h1>
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <form onSubmit={handleSubmit(onSubmit)}>

                                    <div className="row">
                                        <div className="col-md-6">
                                            {/* Columna 1 */}
                                            <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                                                <label htmlFor="weight" className="form-label" style={{ fontWeight: 'bold' }}>Peso</label>
                                                <input {...register('weight')} type="text" className="form-control form-control-user" placeholder="Ingrese el peso" />
                                            </div>

                                            <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                                                <label htmlFor="height" className="form-label" style={{ fontWeight: 'bold' }}>Altura</label>
                                                <input {...register('height')} type="text" className="form-control form-control-user" placeholder="Ingrese la altura" />
                                            </div>
                                            <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                                                <label htmlFor="representative" className="form-label" style={{ fontWeight: 'bold' }}>Representante</label>
                                                <input {...register('representative')} type="text" className="form-control form-control-user" placeholder="Ingrese el representante" />
                                            </div>

                                            <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                                                <label htmlFor="activities" className="form-label" style={{ fontWeight: 'bold' }}>Actividad</label>
                                                <input {...register('activities')} type="text" className="form-control form-control-user" placeholder="Ingrese la actividad" />
                                            </div>

                                            {/* Continúa con el resto de los campos de la columna 1 */}
                                        </div>
                                        <div className="col-md-6">

                                            <div className="row" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>

                                                <label className="form-label" style={{ fontWeight: 'bold' }}>Niños</label>
                                                <select className='form-control' {...register('external_child', { required: true })} onChange={(e) => setValue('external_child', e.target.value)}>
                                                    <option value="">Seleccione un niño</option>
                                                    {Array.isArray(ninos) && ninos.map((mar, i) => (
                                                        <option key={i} value={mar.external_id}>
                                                            {mar.nombres}
                                                        </option>
                                                    ))}
                                                </select>

                                            </div>

                                            <div className="row" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                                                <label className="form-label" style={{ fontWeight: 'bold' }}>Escuelas</label>
                                                <select className='form-control' {...register('external_school', { required: true })} onChange={(e) => setValue('external_school', e.target.value)}>
                                                    <option value="">Seleccione una escuela</option>
                                                    {Array.isArray(escuelas) && escuelas.map((mar, i) => (
                                                        <option key={i} value={mar.external_id}>
                                                            {mar.nombre}
                                                        </option>
                                                    ))}
                                                </select>

                                            </div>

                                            <div className='row' style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>

                                                <label className="form-label" style={{ fontWeight: 'bold' }}>Curso</label>
                                                <select className='form-control' {...register('external_course', { required: true })} onChange={(e) => setValue('external_course', e.target.value)}>
                                                    <option value="">Seleccione un curso</option>
                                                    {Array.isArray(cursos) && cursos.map((mar, i) => (
                                                        <option key={i} value={mar.external_id}>
                                                            {mar.denominacion}
                                                        </option>
                                                    ))}
                                                </select>
                                                {errors.cursos && errors.cursos.type === 'required' && <div className='alert alert-danger'>Seleccione un curso</div>}

                                            </div>

                                        </div>
                                    </div>

                                    <hr />

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div style={{ display: 'flex', gap: '10px', marginTop: '30px' }}>
                                                <a href="/censo" className="btn btn-danger btn-rounded">
                                                    <svg width="16" height="16" fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                                    </svg>
                                                    <span style={{ marginLeft: '5px' }}>Cancelar</span>
                                                </a>
                                                <input className="btn btn-success btn-rounded" type='submit' value='Registrar'></input>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}