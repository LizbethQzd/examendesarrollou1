'use client';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { inicio_sesion } from '@/hooks/Autenticacion';
import { estaSesion } from '@/hooks/SessionUtils';
import mensajes from '@/componentes/Mensajes';
import { useRouter } from 'next/navigation';


export default function Page() {
    const router = useRouter();
    const validationSchema = Yup.object().shape({

        email: Yup.string().required('Ingrese su correo').email('Ingrese un correo'),
        password: Yup.string().required('Ingrese su clave')
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const sendData = (data) => {
        var data = { "email": data.email, "password": data.password, "resource": "login" };
        inicio_sesion(data).then((info) => {
           
            if (!estaSesion()) {
            } else {
                router.push('/principal');
            }
        });
    };
    const blackBorderStyle = {
        border: '4px solid pink',
        padding: '60px',
        borderRadius: '20px'
    };

    return (
        <div className="container">
            <section className="vh-100">
                <div className="container py-5 h-100">
                    <div className="row d-flex align-items-center justify-content-center h-100">
                        <div className="col-md-8 col-lg-7 col-xl-6">
                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                                className="img-fluid" alt="Phone image" />
                        </div>
                        <div className="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                            <form onSubmit={handleSubmit(sendData)}>
                                <div className="mb-4" style={blackBorderStyle}>
                                    <div className="form-outline mb-4">
                                        <label className="form-label"><strong>Correo electrónico</strong></label>
                                        <input {...register('email')} type='text' name="email" id="email" className={`form-control ${errors.email ? 'is-invalid' : ''}`} />
                                        <div className='alert alert-danger invalid-feedback'>{errors.email?.message}</div>
                                    </div>

                                    <div className="form-outline mb-4">
                                        <label className="form-label"><strong>password</strong></label>
                                        <input {...register('password')} type='password' name="password" id="password" className={`form-control ${errors.password ? 'is-invalid' : ''}`} />
                                        <div className='alert alert-danger invalid-feedback'>{errors.password?.message}</div>
                                    </div>

                                    <div className="d-grid gap-1">
                                        <button type="submit" className="btn btn-primary btn-block mb-4">
                                            <strong>Acceder</strong>
                                        </button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );



}
